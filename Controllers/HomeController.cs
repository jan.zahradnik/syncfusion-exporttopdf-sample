﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LinuxDocker.Models;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIORenderer;
using Syncfusion.Pdf;

namespace LinuxDocker.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult WordToPDF(string button)
        {

            string fileLoadTime = "";
            string domLoadTime = "";
            string conversionTime = "";
            string saveTime = "";
            if (button == null)
                return View("Index");

            if (Request.Form.Files != null)
            {
                if (Request.Form.Files.Count == 0)
                {
                    ViewBag.Message = string.Format("Browse a Word document and then click the button to convert as a PDF document");
                    return View("Index");
                }
                // Gets the extension from file.
                string extension = Path.GetExtension(Request.Form.Files[0].FileName).ToLower();
                // Compares extension with supported extensions.
                if (extension == ".doc" || extension == ".docx" || extension == ".dot" || extension == ".dotx" || extension == ".dotm" || extension == ".docm"
                   || extension == ".xml" || extension == ".rtf")
                {
                    MemoryStream stream = new MemoryStream();
                    Request.Form.Files[0].CopyTo(stream);
                    try
                    {
                        //Open using Syncfusion
                        WordDocument document = new WordDocument(stream, FormatType.Automatic);
                        //document.FontSettings.SubstituteFont += SubstitueFont;
                        stream.Dispose();
                        stream = null;
                        // Creates a new instance of DocIORenderer class.
                        DocIORenderer render = new DocIORenderer();
                        // Converts Word document into PDF document.
                        PdfDocument pdf = render.ConvertToPDF(document);
                        MemoryStream memoryStream = new MemoryStream();
                        // Save the PDF document.


                        //Save using Syncfusion
                        pdf.Save(memoryStream);
                        memoryStream.Position = 0;
                        ViewBag.OS = string.Format(System.Environment.OSVersion.ToString());
                        ViewBag.Load = string.Format("FileLoadTime\t" + fileLoadTime);
                        ViewBag.DomLoad = "DomLoadTime\t" + domLoadTime;
                        ViewBag.Conversion = "ConversionTime\t" + conversionTime;
                        ViewBag.Save = "SaveTime\t" + saveTime;
                        return File(memoryStream, "application/pdf", "WordToPDF_SyncfusionDocker.pdf");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = string.Format(ex.Message);
                        //ViewBag.Message = string.Format("The input document could not be processed completely, Could you please email the document to support@syncfusion.com for troubleshooting.");
                    }
                }
                else
                {
                    ViewBag.Message = string.Format("Please choose Word format document to convert to PDF");
                }
            }
            else
            {
                ViewBag.Message = string.Format("Browse a Word document and then click the button to convert as a PDF document");
            }
            return View("Index");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
