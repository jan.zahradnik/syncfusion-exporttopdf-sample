#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0.0-buster-slim AS base
RUN apt-get update -y && apt-get install libfontconfig -y
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0.100-buster-slim AS build
WORKDIR /src
COPY ["LinuxDocker.csproj", ""]
RUN dotnet restore "./LinuxDocker.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "LinuxDocker.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "LinuxDocker.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "LinuxDocker.dll"]